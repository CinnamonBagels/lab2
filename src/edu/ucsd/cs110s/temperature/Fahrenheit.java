/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author SamKo
 *
 */
public class Fahrenheit extends Temperature 
{ 
 public Fahrenheit(float t) 
 { 
 super(t); 
 } 
 public String toString() 
 { 
 // TODO: Complete this method 
	 return Float.toString(this.getValue());
 }
 
@Override
public Temperature toCelsius() {
	// TODO Auto-generated method stub
	Temperature temp = new Celsius((this.getValue() - (float)32) * ((float)5/(float)9));
	return temp;
}
@Override
public Temperature toFahrenheit() {
	// TODO Auto-generated method stub
	return this;
} 
} 


