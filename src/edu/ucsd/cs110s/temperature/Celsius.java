package edu.ucsd.cs110s.temperature;

import java.lang.String;
import java.lang.Float;
/**
 * @author SamKo
 *
 */
public class Celsius extends Temperature 
{ 
 public Celsius(float t) 
 { 
 super(t);  
 } 
 
 public String toString() 
 { 
 return Float.toString(this.getValue());
 }

@Override
public Temperature toFahrenheit() {
	Temperature temp = new Fahrenheit(this.getValue() * ((float)9/(float)5) + (float)32);
	return temp;
}

@Override
public Temperature toCelsius() {
	return this;
} 
} 
